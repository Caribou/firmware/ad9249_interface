----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/10/2020 08:53:16 AM
-- Design Name: 
-- Module Name: AD9249_model - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AD9249_model is
  Port ( 
  
  ADC_FC_P                                    : OUT STD_LOGIC_VECTOR(1 downto 0);
  ADC_FC_N                                    : OUT STD_LOGIC_VECTOR(1 downto 0);

  
  ADC_DC_P                                    : OUT STD_LOGIC_VECTOR(1 downto 0);
  ADC_DC_N                                    : OUT STD_LOGIC_VECTOR(1 downto 0);

  ADC_DOUT_P                                 : OUT STD_LOGIC_VECTOR(15 downto 0);
  ADC_DOUT_N                                 : OUT STD_LOGIC_VECTOR(15 downto 0);
  
  dclk_in                                   : IN STD_LOGIC;
  fclk_in                                   : IN STD_LOGIC
  );
end AD9249_model;

architecture Behavioral of AD9249_model is

signal dclk_p : std_logic;
signal dclk_n : std_logic;
signal fclk_p : std_logic;
signal fclk_n : std_logic;

signal testwrd : std_logic_vector(13 downto 0):= b"01010101010101";
signal dcnt : integer := 0;

signal wrd0 : std_logic_vector(15 downto 0) := x"0000" ;
signal wrd1 : std_logic_vector(15 downto 0) := x"0001" ;
signal wrd2 : std_logic_vector(15 downto 0) := x"0002" ;
signal wrd3 : std_logic_vector(15 downto 0) := x"0003" ;
signal wrd4 : std_logic_vector(15 downto 0) := x"0004" ;
signal wrd5 : std_logic_vector(15 downto 0) := x"0005" ;
signal wrd6 : std_logic_vector(15 downto 0) := x"0006" ;
signal wrd7 : std_logic_vector(15 downto 0) := x"0007" ;



begin

dclk_p <= dclk_in;
dclk_n <= not dclk_in;
fclk_p <= fclk_in;
fclk_n <= not fclk_in;

ADC_DC_P <= dclk_p & dclk_p;
ADC_DC_N <= dclk_n & dclk_n;
ADC_FC_P <= fclk_p & fclk_p;
ADC_FC_N <= fclk_n & fclk_n;




  process (dclk_in,fclk_in)
  begin 
    if rising_edge(fclk_in) then
        dcnt<=0;
        --ADC_DOUT_P <= (others => testwrd(dcnt));--wrd0(dcnt) & wrd1(dcnt) & wrd2(dcnt) & wrd3(dcnt) & wrd4(dcnt) & wrd5(dcnt) & wrd6(dcnt) & wrd7(dcnt) & wrd0(dcnt) & wrd1(dcnt) & wrd2(dcnt) & wrd3(dcnt) & wrd4(dcnt) & wrd5(dcnt) & wrd6(dcnt) & wrd7(dcnt);
        --ADC_DOUT_N <= (others => not testwrd(dcnt)); --(wrd0(dcnt) & wrd1(dcnt) & wrd2(dcnt) & wrd3(dcnt) & wrd4(dcnt) & wrd5(dcnt) & wrd6(dcnt) & wrd7(dcnt) & wrd0(dcnt) & wrd1(dcnt) & wrd2(dcnt) & wrd3(dcnt) & wrd4(dcnt) & wrd5(dcnt) & wrd6(dcnt) & wrd7(dcnt));
    elsif dclk_in'event then
           wrd0 <= wrd0 + 8;
           wrd1 <= wrd1 + 8;
           wrd2 <= wrd2 + 8;
           wrd3 <= wrd3 + 8;
           wrd4 <= wrd4 + 8;
           wrd5 <= wrd5 + 8;
           wrd6 <= wrd6 + 8;
           wrd7 <= wrd7 + 8;
           dcnt <= dcnt +1;
     end if;
  end process;

ADC_DOUT_P <= wrd0(dcnt) & wrd1(dcnt) & wrd2(dcnt) & wrd3(dcnt) & wrd4(dcnt) & wrd5(dcnt) & wrd6(dcnt) & wrd7(dcnt) & wrd0(dcnt) & wrd1(dcnt) & wrd2(dcnt) & wrd3(dcnt) & wrd4(dcnt) & wrd5(dcnt) & wrd6(dcnt) & wrd7(dcnt);
ADC_DOUT_N <= not (wrd0(dcnt) & wrd1(dcnt) & wrd2(dcnt) & wrd3(dcnt) & wrd4(dcnt) & wrd5(dcnt) & wrd6(dcnt) & wrd7(dcnt) & wrd0(dcnt) & wrd1(dcnt) & wrd2(dcnt) & wrd3(dcnt) & wrd4(dcnt) & wrd5(dcnt) & wrd6(dcnt) & wrd7(dcnt));
  
end Behavioral;
