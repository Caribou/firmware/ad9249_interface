--Hongbin@Sep2014
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


library UNISIM;
use UNISIM.VComponents.all;


entity dclk_delay is
port(
      RESET      : in std_logic;
	  DCLKP_IN   : in std_logic;
	  DCLKN_IN   : in std_logic;
	  REFCLK_IN  : in std_logic;
	  
	  --Control ports
	  CNT_OUT    : out std_logic_vector(4 downto 0);
	  CTRL_RDY   : out std_logic;
	  IDLY_LD    : in  std_logic;
	  CNT_IN     : in  std_logic_vector(4 downto 0);
	 	  
	  DCLK_OUT   :out std_logic
	  );
end dclk_delay;

architecture Behavioral of dclk_delay is

--dclk buffered by IBUFDS
signal dclk_buf :std_logic;
signal dclk_delay_ubuf :std_logic;
signal dclk_delay_buf  :std_logic;
signal dclk_delay_ref :std_logic;

--IDELAYE2 associate signals
--signal idely_c           :std_logic; --All control inputs to IDELAYE2 primitive (RST , CE, and INC) are synchronous to the clock input (C). 
--signal idely_ce          :std_logic; --Active high enable for increment/decrement function.
signal idely_cinvctrl   :std_logic := '0'; --Used for dynamically switching the polarity of C pin. 
signal idely_datain     :std_logic := '0'; 
signal idely_inc        :std_logic := '0'; --INC = 1 increments when CE is high. INC=0 decrements.
signal idely_ldpipeen   :std_logic := '0'; --When High, loads the pipeline register with the value currently on the CNTVALUEIN pins
signal idely_regrst     :std_logic := '0'; --When high, resets the pipeline register to all zeros. Only used in "V AR_LOAD_PIPE" mode.

begin


DCLK_BUFFER: IBUFDS
   generic map (
      DIFF_TERM => TRUE, -- Differential Termination 
      IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "DEFAULT")
   port map (
      O => dclk_buf,  -- Buffer output
      I => DCLKP_IN,  -- Diff_p buffer input (connect directly to top-level port)
      IB => DCLKN_IN -- Diff_n buffer input (connect directly to top-level port)
   );
		     
   
   IDELAYCTRL_inst : IDELAYCTRL
   port map (
      RDY => CTRL_RDY,       -- 1-bit output indicates validity of the REFCLK
      REFCLK => REFCLK_IN, -- 1-bit reference clock input
      RST => RESET        -- 1-bit reset input------------------------------HIGH ACTIVE, MUST RESET AFTER CONFIGURATION
   );  
   

	-- IDELAYE2: Input Fixed or Variable Delay Element
	-- 7 Series
	-- Xilinx HDL Libraries Guide, version 2014.2
	IDELAYE2_inst : IDELAYE2
	generic map (
	CINVCTRL_SEL => "FALSE",          -- Enable dynamic clock inversion (FALSE, TRUE)
	DELAY_SRC => "IDATAIN",           -- Delay input (IDATAIN, DATAIN)
	HIGH_PERFORMANCE_MODE => "TRUE",  -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
	IDELAY_TYPE => "VAR_LOAD",        -- FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
	IDELAY_VALUE => 0,                -- Input delay tap setting (0-31)
	PIPE_SEL => "FALSE",              -- Select pipelined mode, FALSE, TRUE
	REFCLK_FREQUENCY => 200.0,        -- IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
	SIGNAL_PATTERN => "CLOCK"         -- DATA, CLOCK input signal
	)
	port map (
	CNTVALUEOUT => CNT_OUT, -- 5-bit output: Counter value output
	DATAOUT => dclk_delay_ubuf,       -- 1-bit output: Delayed data output
	C => dclk_delay_ref,              -- 1-bit input: Clock input
	CE => '0',--idely_ce,             -- 1-bit input: Active high enable increment/decrement input
	CINVCTRL => idely_cinvctrl,       -- 1-bit input: Dynamic clock inversion input
	CNTVALUEIN => CNT_IN,             -- 5-bit input: Counter value input
	DATAIN => idely_datain,           -- 1-bit input: Internal delay data input
	IDATAIN => dclk_buf,              -- 1-bit input: Data input from the I/O
	INC => idely_inc,                 -- 1-bit input: Increment / Decrement tap delay input
	LD => IDLY_LD,                    -- 1-bit input: Load IDELAY_VALUE input
	LDPIPEEN => idely_ldpipeen,       -- 1-bit input: Enable PIPELINE register to load data input
	REGRST => idely_regrst            -- 1-bit input: Active-high reset tap-delay input
	);
	-- End of IDELAYE2_inst instantiation

BUFR_inst : BUFR
   generic map (
      BUFR_DIVIDE => "BYPASS", -- Values: "BYPASS", "1", "2", "3", "4", "5", "6", "7", "8" 
      SIM_DEVICE => "7SERIES"  -- Must be set to "7SERIES"  
   )
   port map (
      O => dclk_delay_ref,     -- 1-bit output: Clock buffer output
      CE => '1',   -- 1-bit input: Active high clock enable input
      CLR => RESET, -- 1-bit input: Active high reset input
      I => dclk_delay_ubuf      -- 1-bit input: Clock buffer input driven by an IBUFG, MMCM or local interconnect
   );	
 
  
BUFG_inst: BUFG
port map (
O => dclk_delay_buf, -- 1-bit output: Clock output
I => dclk_delay_ref -- 1-bit input: Clock input
);
   

DCLK_OUT <= dclk_delay_buf;


end Behavioral;

